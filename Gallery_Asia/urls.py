from django.conf.urls import patterns, include, url
from django.conf import settings                ##############
from django.conf.urls.static import static      # For developing server - media


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'Gallery_Asia.views.home', name='home'),
    # url(r'^Gallery_Asia/', include('Gallery_Asia.foo.urls')),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),

) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

import gallery.views

urlpatterns += patterns('gallery.views',
    url(r'^$', 'home'),
    url(r'^upload_form/?$', 'upload_form'),
    url(r'^upload_success/?', 'upload_success'),
    url(r'^galleries/?$', 'galleries'),
    url(r'^contact$', 'contact'),
    url(r'^about$', 'about'),

    url(r'^galleries/(?P<category_slug>[\w]+)$', gallery.views.GalleryListView.as_view()),
    url(r'^galleries/(?P<category_slug>[\w]+)/(?P<gallery_slug>[\w\-]+)$', gallery.views.PhotosListView.as_view())
)