__author__ = 'konrad'
from django import forms
from gallery.models import Photo, Gallery, Category


class UploadFilesForm(forms.Form):
    file = forms.FileField()
    categories = forms.ModelChoiceField(queryset=Category.objects.all(), empty_label="<blank>")
    gallery = forms.CharField(max_length=100)
    #date = forms.DateInput()