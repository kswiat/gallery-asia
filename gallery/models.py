from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100)

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name_plural = 'Categories'


class Photo(models.Model):
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    # image = models.ImageField()

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name_plural = 'Photos'


class Gallery(models.Model):
    category = models.ForeignKey(Category)
    photos = models.ManyToManyField(Photo)
    name = models.CharField(max_length=100)
    slug = models.SlugField()
    date = models.DateField(null=True)
    # emblem = models.ImageField()

    def __unicode__(self):
        return u'%s' % self.name

    class Meta:
        verbose_name_plural = 'Galleries'


