__author__ = 'konrad'
from django.shortcuts import render, HttpResponseRedirect, get_list_or_404
from .forms import UploadFilesForm
from .models import Category, Gallery, Photo
from django.views.generic import ListView
from django.http import Http404


def home(request):
    active_menu_link = 'home'
    return render(request, 'home.html', {'active_menu_link': active_menu_link})


def upload_form(request):
    """

    :param request:
    :return: upload form or success template
    """
    admin = False
    if request.method == 'POST':
        form = UploadFilesForm(request.POST, request.FILES)
        category_id = request.POST['categories']
        category = Category.objects.get(id=category_id)
        gallery = request.POST['gallery']
        new_gallery = Gallery.objects.create(name=gallery, category=category, slug=gallery)
        # TODO: uploaded file type, size, unique,  validation
        # TODO: other form fields validation

        if form.is_valid():
            for file in request.FILES.getlist('file'):
                handle_uploaded_file(file, new_gallery)
            admin = True
            return HttpResponseRedirect('upload_success')
    else:
        form = UploadFilesForm()
    return render(request, 'upload_form.html', {'form': form, 'admin': admin})


def handle_uploaded_file(file_, new_gallery):
    """


    :rtype : does not return anything
    :param f: request.FILES
    """
    uploaded_file = file_.name
    destination_path = '/tmp/%s' % (uploaded_file)
    destination = open(destination_path, 'wb+')
    for chunk in file.chunks():
    #if uploaded_file > 2.5MiB (multiple_chunks() is True), have to use chunks() in loop instead of read()
        destination.write(chunk)
    destination.close()
    new_photo = Photo.objects.create(name=uploaded_file, slug=uploaded_file)
    new_gallery.photos.add(new_photo.id)
    new_gallery.save()


def upload_success(request):
    return render(request, 'upload_success.html', {})


def galleries(request):
    """

    :param request:
    :return: Galleries container page template
    """
    categories = list(Category.objects.all())
    active_menu_link = 'galleries'
    return render(request, 'galleries.html', {'categories': categories, 'active_menu_link': active_menu_link})

#
# def landscape(request):
#     """
#
#     :param request:
#     :return: Landscape sub-gallery template
#     """
#     active_menu_link = 'galleries'
#     category_landscape = Category.objects.get(name='landscape')
#     galleries_landscape = category_landscape.gallery_set.all()
#     return render(request, 'landscape.html', {'galleries': galleries_landscape, 'active_menu_link': active_menu_link})
#
#
# def equine(request):
#     """
#
#     :param request:
#     :return: Equine sub-galleries template
#     """
#     active_menu_link = 'galleries'
#     category_equine = Category.objects.get(name='equine')
#     galleries_equine = category_equine.gallery_set.all()
#     return render(request, 'equine.html', {'galleries': galleries_equine, 'active_menu_link': active_menu_link})
#
#
# def nature(request):
#     """
#
#     :param request:
#     :return: Nature sub-gallery template
#     """
#     active_menu_link = 'galleries'
#     category_nature = Category.objects.get(name='nature')
#     galleries_nature = category_nature.gallery_set.all()
#     return render(request, 'nature.html', {'galleries': galleries_nature, 'active_menu_link': active_menu_link})
#
#
# def people(request):
#     """
#
#     :param request:
#     :return: People sub-gallery template
#     """
#     active_menu_link = 'galleries'
#     category_people = Category.objects.get(name='people')
#     galleries_people = category_people.gallery_set.all()
#     return render(request, 'people.html', {'galleries': galleries_people, 'active_menu_link': active_menu_link})
#
#
# def events(request):
#     """
#
#     :param request:
#     categor
#     :return: Events sub-gallery template
#     """
#     active_menu_link = 'galleries'
#     category_events = Category.objects.get(name='events')
#     galleries_events = category_events.gallery_set.all()
#     return render(request, 'events.html', {'galleries': galleries_events, 'active_menu_link': active_menu_link})
#
#
# def other(request):
#     """
#
#     :param request:
#     :return: Other sub-gallery template
#     """
#     active_menu_link = 'galleries'
#     category_other = Category.objects.get(name='other')
#     galleries_other = category_other.gallery_set.all()
#     return render(request, 'other.html', {'galleries': galleries_other, 'active_menu_link': active_menu_link})
#

def contact(request):
    """

    :param request:
    :return: Contact page template
    """
    active_menu_link = 'contact'
    return render(request, 'contact.html', {'active_menu_link': active_menu_link})


def about(request):
    """

    :param request:
    :return: About page template
    """
    active_menu_link = 'about'
    return render(request, 'about.html', {'active_menu_link': active_menu_link})


class PhotosListView(ListView):
    template_name = 'gallery/photos_in_gallery_view.html'
    paginate_by = 5
    model = Photo

    def get_context_data(self, **kwargs):
        context = super(PhotosListView, self).get_context_data(**kwargs)
        try:
            context['category'] = self.kwargs['category_slug']
            context['gallery'] = self.kwargs['gallery_slug']
            context['photos'] = Gallery.objects.get(name=context['gallery']).photos.all()
            return context
        except:
            raise Http404


class GalleryListView(ListView):
    model = Gallery
    template_name = 'gallery/galleries_in_categories_view.html'
    def get_context_data(self, **kwargs):
        context = super(GalleryListView, self).get_context_data(**kwargs)
        try:
            context['category'] = self.kwargs['category_slug']
            context['galleries'] = Category.objects.get(name=context['category']).gallery_set.all()
            return context
        except:
            raise Http404


